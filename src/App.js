import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route} from 'react-router-dom'

import Login from './views/Login';
import Signup from './views/Signup';
import Dashboard from './views/Dashboard';

function App() {
  return (
  <>
    <Router>
      
      <Switch>
        <Route path='/login' component={Login}></Route>
      </Switch>
      <Switch>
        <Route path='/signup' component={Signup}></Route>
      </Switch>
      <Switch>
        <Route path='/dashboard/ipc'> <Dashboard page='ipc'/> </Route>
      </Switch>
      <Switch>
        <Route path='/'>
          <Login />
        </Route>
      </Switch>
    </Router>
    
  </>    
  );
}

export default App;
