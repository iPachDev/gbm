
import { Grid} from 'semantic-ui-react';

import MenuSide from './partials/MenuSide';
import ChartIPC from './partials/ChartIPC';

const Dashboard = ({page}) => {
        
    const getComponent = (comp) => {
        switch(comp){
            case 'ipc':
                return <ChartIPC />
            break;
            default : 
                return <h2>Selecciona un opción</h2>
            break;
        }
    }

    return ( <>


        <Grid style={{height:'calc(100vh + 14px)'}}>
            <Grid.Column width={2} style={{borderRight:'1px solid #1B1C1D'}}>
                <MenuSide />
            </Grid.Column>
            <Grid.Column style={{backgroundColor:'#F7F7F7'}} width={14}>
                {getComponent(page)}
            </Grid.Column>
        </Grid>

    
    </> );
}
 
export default Dashboard;