import { Grid, Header, Form, Button, Message } from 'semantic-ui-react'

const Login = () => {

    const validateLogin = () => {
        window.location = '/dashboard/ipc'
    }

    return (<>
        <Grid columns={2} style={{height:'calc(100vh + 14px)'}}>
            <Grid.Column style={{background : '#000000'}}>
                <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                    <Grid.Column style={{ maxWidth: 450 }}> 
                        <Header as='h1' style={{color:'#FFF'}}>Bienvenido a GBM Challenge</Header>
                    </Grid.Column>
                </Grid>
            </Grid.Column>
            <Grid.Column>
                <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                    <Grid.Column style={{ maxWidth: 450 }}> 
                        <Form size='large'>
                            <Header as='h1'>Inicio de Sesión</Header>
                            <p>Bienvenido al vizor de IPC para GBM, ingresa tus credenciales para acceder</p>
                            <Form.Input fluid icon='mail' iconPosition='left' placeholder='mail@mail.com' id='userMail'/>
                            <Form.Input
                                fluid
                                icon='lock'
                                iconPosition='left'
                                placeholder='* * * * * * * '
                                type='password'
                                id='userPassword'
                            />

                            <Button color='black' fluid size='large' onClick={validateLogin}>
                                Iniciar Sesión
                            </Button>
                            <Message>¿Aún no tienes cuenta? <a href='/signup'>Registrate aquí</a></Message>
                        </Form>
                    </Grid.Column>
                </Grid>
                
            </Grid.Column>
        </Grid>          
    </>);
}
 
export default Login;