import { Grid, Header, Form, Button, Message } from 'semantic-ui-react'

const Signup = () => {
    return (<>
        <Grid columns={2} style={{height:'calc(100vh + 14px)'}}>
            <Grid.Column>
                <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                    <Grid.Column style={{ maxWidth: 450 }}> 
                        <Form size='large'>
                            <Header as='h1'>Registrarme</Header>
                            <p>Bienvenido al vizor de IPC si aun no tienes cuenta por favor llena el siguiente formulario.</p>
                            <Form.Input fluid icon='user' iconPosition='left' placeholder='Nombre' id='userMail'/>
                            <Form.Input fluid icon='mail' iconPosition='left' placeholder='mail@mail.com' id='userMail'/>
                            <Form.Input
                                fluid
                                icon='lock'
                                iconPosition='left'
                                placeholder='* * * * * * * '
                                type='password'
                                id='userPassword'
                            />

                            <Button color='black' fluid size='large'>
                                Iniciar Sesión
                            </Button>
                            <Message>¿Ya tienes cuenta? <a href='/login'>Iniciar Sesión</a></Message>
                        </Form>
                    </Grid.Column>
                </Grid>
                
            </Grid.Column>
            <Grid.Column style={{background : '#000000'}}>
                <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                    <Grid.Column style={{ maxWidth: 450 }}> 
                        <Header as='h1' style={{color:'#FFF'}}>Registrarme en GBM Challenge</Header>
                    </Grid.Column>
                </Grid>
            </Grid.Column>
        </Grid>          
    </>);
}
 
export default Signup;