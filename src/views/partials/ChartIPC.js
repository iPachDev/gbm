import React, {useEffect, useState} from 'react';
import { Line } from 'react-chartjs-2';
import axios from 'axios';
import moment from 'moment';

const ChartIPC = () => {

    const endPoint = 'https://run.mocky.io/v3/cc4c350b-1f11-42a0-a1aa-f8593eafeb1e'

    const [data, setData ] = useState();

    const getDataIPC = async () => {
        const response = await axios.get(endPoint);
        console.log(response.data);

        const dataToLabel = response.data.map((x) => {
            return x.date;
        });

        const dataToDataSet = response.data.map((x) => {
            return x.price;
        });

        console.log(dataToLabel, dataToDataSet);
        setData({
            labels: dataToLabel,
            datasets: [
              {
                label: 'IPC',
                data: dataToDataSet,
                fill: false,
                // backgroundColor: 'rgb(255, 99, 132)',
                // borderColor: 'rgba(255, 99, 132, 0.2)',
              },
            ],
          });
    }

    useEffect(async () => {
        await getDataIPC();
    }, []);

    return ( <>
        <div style={{margin:20}}>
            <h1>Índice de Precios y Cotizaciones</h1>
            <Line data={data}  />
        </div>
    </> );
}
 
export default ChartIPC;